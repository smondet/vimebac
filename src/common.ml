include Base
include Caml.Printf
module Hashtbl = Caml.Hashtbl

module Float = struct
  include Base.Float

  let float = of_int
  let truncate = Caml.truncate
  let floor = Caml.floor
  let ( + ) = ( +. )
  let ( - ) = ( -. )
  let ( * ) = ( *. )
  let ( / ) = ( /. )
  let pi = 4.0 * atan 1.
  let mid a b = (a + b) / 2.
end
